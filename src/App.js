import {Component} from "react";
import Person from "./demo/Person";

class App extends Component {
  state = {
    people: [
      {name: "Azat", age: "18"},
      {name: "John", age: "30"}
    ]
  };

  changeName = () => {
    // this.setState({
    //   people: [
    //     {name: "Azat", age: "18"},
    //     {name: "Not John", age: "30"},
    //   ]
    // });

    // const peopleCopy = [...this.state.people];
    // const personCopy = {...peopleCopy[1]};
    // personCopy.name = 'Not John';
    // peopleCopy[1] = personCopy;

    this.setState({
      people: this.state.people.map((p, i) => {
        if (i === 1) {
          return {...p, name: "Not John"};
        }

        return p;
      }),
    });

    console.log(this.state.people[1]);
  };

  render() {
    return (
        <div>
          <Person
              name={this.state.people[0].name}
              age={this.state.people[0].age}
          >
            Hobby: <b>Coding</b>
          </Person>
          <Person
              name={this.state.people[1].name}
              age={this.state.people[1].age}>
            Hobby: <b>Alcohol</b>
          </Person>
          <div>
            <button onClick={this.changeName}>Change name</button>
          </div>
        </div>
    );
  }
}

export default App;
